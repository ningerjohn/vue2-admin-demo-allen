export default {
  namespaced: true,
  state: {
    token: localStorage.getItem('token') || null, // token
  },
  getters: {
    checkIsLogin: (state) => {
      return state.token !== null
    },
  },
  actions: {},
  mutations: {
    setToken(state, token) {
      state.token = token
      localStorage.setItem('token', token)
    },
    logOut(state) {
      state.token = null
      localStorage.removeItem('token')
      localStorage.removeItem('menuData')
    },
  },
}
