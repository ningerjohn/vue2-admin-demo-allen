import Vue from "vue"
import Vuex from 'vuex'
import layout from '@/store/layout'
import entry from '@/store/entry'

Vue.use(Vuex)


export default new Vuex.Store({
    modules: {
        layout,
        entry
    }
})


