import { getMinePermission } from '@/api'
import router from '@/router'

// 将菜单数据处理成路由数据
const prepareRoutesData = (menuData) => {
  let resData = []
  if (menuData.length > 0) {
    menuData.forEach((item) => {
      let routeItem = {}
      routeItem = {
        name: item.mark,
        path: item.path || '',
      }
      if (item.component) {
        routeItem['component'] = () => import(`../../views/${item.component}`)
      }
      if (item.children) {
        routeItem['children'] = prepareRoutesData(item.children)
      }
      resData.push(routeItem)
    })
  }
  return resData
}
// 动态注册路由
const registerDynamicRoutes = (routes) => {
  // 开始动态注册路由
  if (routes.length > 0) {
    routes.forEach((item) => {
      console.log('动态路由条目：', item)
      if (item.children) {
        router.addRoute(item)
      } else {
        router.addRoute('index', item)
      }
    })
  }
  // 403无权限
  router.addRoute({
    name: 'layout',
    path: '/',
    component: () => import('../../views/layouts/default/Main'),
    children: [
      {
        name: 'nopermission',
        path: 'nopermission',
        component: () => import('../../views/extra/NoPermission'),
      },
    ],
  })
  router.addRoute({ path: '*', redirect: '/nopermission' }) // 匹配不到
  console.log('路由注册完毕', routes)
}

export default {
  namespaced: true,
  state: {
    // 控制菜单展开还是收起
    isMenuCollapse: false,
    // 菜单数据
    menuData: JSON.parse(localStorage.getItem('menuData')) || [],
    routesInitialized: false, // 用于标记路由是否已注册
  },
  // dispatch - actions
  actions: {
    // 同步获取菜单数据
    async queryMenuData({ commit }) {
      try {
        const { data } = await getMinePermission()
        commit('setMenuData', data.menu)
      } catch (err) {
        console.error(err)
      }
    },
    // 初始化路由
    initializeRoutes({ state }) {
      // ES6语法：从context里面析构出来state和commit
      console.log('初始化路由')
      if (!state.routesInitialized) {
        console.log('第一次初始化路由')
        state.routesInitialized = true
        let routesConfig = prepareRoutesData(state.menuData)
        registerDynamicRoutes(routesConfig)
      }
    },
  },
  // commit - mutations
  mutations: {
    collapseMenu(state) {
      state.isMenuCollapse = !state.isMenuCollapse
    },
    // 设置菜单数据
    setMenuData(state, menuData) {
      state.menuData = menuData
      localStorage.setItem('menuData', JSON.stringify(menuData))
    },
    // 动态注册路由
    // registerRoutes(menus) {
    // // 开始动态注册路由
    // if (menus.length > 0) {
    //   menus.forEach((item) => {
    //     if (item.path) {
    //       const routeItem = {
    //         name: item.mark,
    //         path: item.path,
    //         component: () => import(`../../views/${item.component}`),
    //       }
    //       console.log('routeItem', routeItem, `../../views/${item.component}`)
    //       router.addRoute('index', routeItem)
    //     } else if (item.children) {
    //       this.commit('registerRoutes', item.children)
    //     }
    //   })
    // }
    // },
  },
}
