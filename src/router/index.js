import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

const router = new VueRouter({
  routes: [
    {
      name: 'index',
      path: '/',
      redirect: '/home',
      component: () => import('@/views/layouts/default/Main'),
      children: [
        // {
        //   // 内容管理
        //   name: 'content',
        //   path: '',
        //   // component: () => import('@/views/layouts/default/Main'),
        //   children: [
        //     {
        //       name: 'article',
        //       path: '/content/article',
        //       component: () => import('@/views/content/Article'),
        //     },
        //   ],
        // },
        // {
        //   name: 'home',
        //   path: 'home',
        //   component: () => import('@/views/Home'),
        // },
        // {
        //   path: '/other/one',
        //   component: () => import('@/views/other/One'),
        // },
        // {
        //   path: 'other/two',
        //   component: () => import('@/views/other/Two'),
        // },
        // {
        //   name: 'article',
        //   path: 'content/article',
        //   component: () => import('@/views/content/Article'),
        // },
        // {
        //   name: 'user',
        //   path: 'system/user',
        //   component: () => import('@/views/system/User'),
        // },
        // {
        //   name: 'role',
        //   path: 'system/role',
        //   component: () => import('@/views/system/Role'),
        // },
        // {
        //   name: 'permission',
        //   path: 'system/permission',
        //   component: () => import('@/views/system/Permission'),
        // },
      ],
    },
    // { // 内容管理
    //     name: 'content',
    //     path: '/content',
    //     component: () => import('@/views/layouts/default/Main'),
    //     children: [
    //         {
    //             name: 'article',
    //             path: '/content/article',
    //             component: () => import('@/views/content/Article')
    //         },
    //     ]
    // },
    //   { // 首页
    //     name: 'home',
    //     path: '/',
    //     component: () => import('@/views/layouts/default/Main'),
    //     children: [
    //         {
    //             name: 'homepage',
    //             path: '/home',
    //             component: () => import('@/views/Home')
    //         },
    //     ]
    // },
    // { // 系统管理
    //     name: 'system',
    //     path: '/system',
    //     component: () => import('@/views/layouts/default/Main'),
    //     children: [
    //         {
    //             name: 'user',
    //             path: 'user',
    //             component: () => import('@/views/system/User')
    //         },
    //         {
    //             name: 'role',
    //             path: 'role',
    //             component: () => import('@/views/system/Role')
    //         },
    //         {
    //             name: 'permission',
    //             path: 'permission',
    //             component: () => import('@/views/system/Permission')
    //         },
    //     ]
    // },
    {
      name: 'login',
      path: '/login',
      component: () => import('@/views/entry/Login'),
    },
    // {
    //   name: 'layout',
    //   path: '/',
    //   component: () => import('@/views/layouts/default/Main'),
    //   children: [
    //     {
    //       name: 'nopermission',
    //       path: 'nopermission',
    //       component: () => import('@/views/extra/NoPermission'),
    //     },
    //   ],
    // }, // 无权限提示页
    // {
    //   path: '*',
    //   redirect: '/nopermission',
    // },
  ],
})

router.beforeEach((to, from, next) => {
  const isLoggedIn = store.getters['entry/checkIsLogin'] // 检查用户是否登录的逻辑
  if (!isLoggedIn && to.path !== '/login') { // 未登录访问非登录页的话，就强制跳转登录页
    next('/login')
  }
  if (to.matched.length === 0) {
    console.log('没有匹配到路由：', to.fullPath)
    // 如果没有匹配到路由
    if (isLoggedIn) {
      console.log('登录了，那就先检查是否注册过路由:', store.state.layout.routesInitialized)
      if (!store.state.layout.routesInitialized) {
        console.log('没有注册过路由，那就开始添加路由')
        store.dispatch('layout/initializeRoutes').then(() => {
          // console.log('路由注册以后:', router.getRoutes())
          // next({...to, replace: true})
          next({ path: to.fullPath })
        })
      }
      console.log('注册以后的路由:', router.getRoutes())
      // next() // 重定向到无权限页面
    } else if (from.path !== '/login') {
      next({ path: '/login' }) // 重定向到登录页面
    }
  } else {
    console.log('匹配到路由了', to.fullPath)
    next() // 继续导航
  }
})

// push Error: Redirected 异常处理
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch((err) => err)
}

// login - home - nopermission
Vue.use(VueRouter)

export default router
