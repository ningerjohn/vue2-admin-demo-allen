/**
 * 对项目中axios请求进行函数化封装
 */

import requestApi from '@/utils/request'

// 用户登录
export const userLogin = (params) => {
  return requestApi({ url: '/entry/login', method: 'post', ...params })
}

// 获取用户菜单权限数据
export const getMineHeaderInfo = () => {
  return requestApi({ url: '/user-manage/get-mine-header-info' })
}

// 获取用户菜单权限数据
export const getMinePermission = () => {
  return requestApi({ url: '/user-manage/mine-permission' })
}

// 请求首页数据的接口
export const getHomeData = () => {
  return requestApi({ url: '/home/get-home-data' })
}

// 请求文章列表的接口
export const queryArticleList = () => {
  return requestApi({ url: '/article-manage/get-article-list' })
}

// 创建新增文章
export const createUpdateArticle = (params) => {
  return requestApi({ url: '/article-manage/create-update-article', method: 'post', ...params })
}

// 切换文章上下架状态
export const toggleArticleStatus = (params) => {
  return requestApi({ url: '/article-manage/toggle-enable-status', method: 'post', ...params })
}

// 删除文章
export const deleteArticle = (params) => {
  return requestApi({ url: '/article-manage/delete-article', method: 'post', ...params })
}





// 查询用户列表
export const queryUserList = (params = {}) => {
  return requestApi({ url: '/user-manage/get-user-list', ...params })
}

// 创建新增用户
export const createUpdateUser = (params) => {
  return requestApi({ url: '/user-manage/create-update-user', method: 'post', ...params })
}

// 切换用户状态
export const toggleUserStatus = (params) => {
  return requestApi({ url: '/user-manage/toggle-enable-status', method: 'post', ...params })
}

// 删除用户
export const deleteUser = (params) => {
  return requestApi({ url: '/user-manage/delete-user', method: 'post', ...params })
}

// 查询角色条目
export const queryRoleItems = (params = {}) => {
  return requestApi({ url: '/role-manage/get-role-items', ...params })
}



// 查询角色列表
export const queryRoleList = (params = {}) => {
  return requestApi({ url: '/role-manage/get-role-list', ...params })
}

// 创建新增角色
export const createUpdateRole = (params) => {
  return requestApi({ url: '/role-manage/create-update-role', method: 'post', ...params })
}

// 删除角色
export const deleteRole = (params) => {
  return requestApi({ url: '/role-manage/delete-role', method: 'post', ...params })
}

// 切换角色状态
export const toggleRoleStatus = (params) => {
  return requestApi({ url: '/role-manage/toggle-enable-status', method: 'post', ...params })
}

// 查询权限树形数据 - 用于角色绑定权限弹窗
export const queryPermissionTreeForRoleBind = (params = {}) => {
  // console.log('查询权限params：', params);
  return requestApi({ url: '/role-manage/get-permission-tree-for-role-bind', ...params })
}

// 查询角色已绑定权限数据 - 用于角色绑定权限弹窗
export const queryRoleBindedPermission = (params = {}) => {
  // console.log('查询权限params：', params);
  return requestApi({ url: '/role-manage/get-role-binded-permission', ...params })
}

// 绑定角色权限
export const bindRolePermission = (params) => {
  return requestApi({ url: '/role-manage/bind-role-permission', method: 'post', ...params })
}

// 查询权限列表
export const queryPermissionList = (params = {}) => {
  return requestApi({ url: '/permission-manage/get-permission-list', ...params })
}

// 查询权限树形图格式列表
export const queryPermissionTreeList = (params = {}) => {
  return requestApi({ url: '/permission-manage/get-permission-tree-list', ...params })
}

// 创建新增权限
export const createUpdatePermission = (params) => {
  return requestApi({ url: '/permission-manage/create-update-permission', method: 'post', ...params })
}

// 删除权限
export const deletePermission = (params) => {
  return requestApi({ url: '/permission-manage/delete-permission', method: 'post', ...params })
}

// 切换权限状态
export const togglePermissionStatus = (params) => {
  return requestApi({ url: '/permission-manage/toggle-enable-status', method: 'post', ...params })
}
