import Mock from 'mockjs'

const data = {
    // 获取用户数据
    getUserList: function () {
        return {
            code: 0,
            msg: '用户数据',
            data: {
                // 列表数据
                list: Mock.mock({
                    'tableData|5':
                        [
                            {
                                'id|+1': 1, // 记录id
                                'avatar|1': '@img(100x100)', // 上架状态
                                'username|1': '@word', // 账号
                                'realname|1': '@cname', // 姓名
                                'role|1': '@word', // 角色
                                'remark|1': '@csentence', // 简介
                                'createTime|': '@date("yyyy-MM-dd HH:mm:ss")', // 发布时间
                                'updateTime|': '@date("yyyy-MM-dd HH:mm:ss")', // 更新时间
                                'enableStatus|1': ['enable', 'disable'], // 启用状态
                            }
                        ]
                }).tableData,
                page: { // 分页数据
                    total: 100,
                    pageSize: 10,
                    pageNumber: 1
                },
            }
        }
    }
}

export default data