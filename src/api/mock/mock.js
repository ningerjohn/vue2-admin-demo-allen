/**
 * mock拦截并模拟请求响应
 */


import Mock from 'mockjs'
import homeData from '@/api/mock/home'
import content from '@/api/mock/content'
import system from '@/api/mock/system'

// 获取首页数据
Mock.mock('/mock/home/get-home-data', 'get', homeData.getHomeData)

// 获取文章列表数据
Mock.mock('/mock/content/query-article-list', 'get', content.getArticleList)

// 获取文章列表数据
Mock.mock('/mock/system/query-user-list', 'get', system.getUserList)




