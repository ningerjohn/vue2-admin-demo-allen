import Mock from 'mockjs'

const data = {
    // 获取文章数据
    getArticleList: function () {
        return {
            code: 0,
            msg: '文章数据',
            data: {
                // 列表数据
                list: Mock.mock({
                    'tableData|5':
                        [
                            {
                                'id|+1': 1, // 文章id
                                'title|1': '@ctitle', // 标题
                                'brief|1': '@csentence', // 简介
                                'content|5-20': '@csentence', // 内容
                                'author|1': '@cname@clast', // 作者
                                'createTime|': '@date("yyyy-MM-dd HH:mm:ss")', // 发布时间
                                'updateTime|': '@date("yyyy-MM-dd HH:mm:ss")', // 更新时间
                                'enableStatus|1': ['enable', 'disable'], // 启用状态
                                'onlineStatus|1': ['online', 'offline'], // 上架状态
                                'cover|1': '@img(100x100)', // 上架状态
                            }
                        ]
                }).tableData,
                page: { // 分页数据
                    total: 100,
                    pageSize: 10,
                    pageNumber: 1
                },
            }
        }
    }
}

export default data