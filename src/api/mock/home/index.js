import Mock from 'mockjs'

const data = {
    // 获取首页数据
    getHomeData: function () {
        return {
            code: 0,
            msg: '首页数据',
            data: {
                userInfo: { // 登录用户数据
                    name: 'NingerGroup',
                    role: '超级管理员'
                },
                // 表格列表数据
                tableData: Mock.mock({
                    'tableData|10':
                        [
                            {
                                'date|': '@date("yyyy-MM-dd")',
                                'name|1': '@cfirst@clast',
                                'address|1': '@region',
                            }
                        ]
                }).tableData,
                orderData: { // 订单数据

                },
                chartData: { // 图标数据

                },
            }
        }
    }
}

export default data