// 引入element-ui插件
import Vue from 'vue'

import 'element-ui/lib/theme-chalk/index.css'; // 引入elementui的样式文件

// 全量引入
import ElementUI from 'element-ui';
Vue.use(ElementUI)

// // 按需引入
// import {Row, Button} from 'element-ui';
// Vue.use(Row);
// Vue.use(Button);