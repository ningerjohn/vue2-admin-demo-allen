import axios from 'axios'
import router from '@/router'
import { Message } from 'element-ui'

const http = axios.create({
  baseURL: process.env.NODE_ENV == 'development' ? '/v1/' : '/v1/', // 请求地址前缀
  timeout: 5000, // 请求超时5秒
})

// 添加请求拦截器
http.interceptors.request.use(
  function (config) {
    // 发送请求之前做些什么
    config.headers.Authorization = 'Bearer ' + localStorage.getItem('token')
    // 默认显示请求消息内容
    config.showSuccessMsg = typeof config.showSuccessMsg != 'undefined' ? config.showSuccessMsg : true
    config.showErrorMsg = typeof config.showErrorMsg != 'undefined' ? config.showErrorMsg : true
    return config
  },
  function (error) {
    // 对请求错误做些处理
    return Promise.reject(error)
  }
)

// 添加响应拦截器
http.interceptors.response.use(
  (response) => {
    // 对响应数据进行处理
    const data = response.data
    // 如果是get请求，一般是获取数据，code不为0时需要提示
    if (response.config.method == 'get') {
      if (data.code != 0) {
        Message.error(data.msg)
      }
    }
    // 如果是post请求，一般是提交数据行为，需要提示
    if (response.config.method == 'post') {
      if (data.code == 0) {
        console.log('showSuccessMsg', response.config.showSuccessMsg)
        response.config.showSuccessMsg ? Message.success(data.msg) : ''
      } else {
        response.config.showErrorMsg ? Message.error(data.msg + '：' + data.data.error) : ''
      }
    }
    return Promise.resolve(data)
  },
  (error) => {
    // 响应错误处理
    if (error.response.status == 401) {
      Message.error('身份失效，请重新登录')
      setTimeout(() => {
        localStorage.removeItem('token')
        router.push('login')
      }, 500)
      return error.response
    } else {
      // TBC：错误信息无法弹窗提示
      error.response.config.showErrorMsg ? Message.error(error.response.data.msg + '：' + error.response.data.data.error) : ''
    }
    // 对响应错误做些处理
    return Promise.reject(error)
  }
)

function request(options = {}) {
  // 默认get请求
  options.method = options.method || 'get'
  // 注意：对于post请求，data 属性会被作为请求体发送到服务器；而对于get 请求则使用 params 属性
  if (options.method === 'get') {
    options.params = options.data
  }
  return http(options)
}

export default request
