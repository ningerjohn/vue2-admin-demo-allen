import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 引入element-ui插件
import './plugins/elementui'
// 引入路由配置
import router from './router'
// 引入vuex配置
import store from '@/store'
// // 引入mockjs
// import '@/api/mock/mock'

new Vue({
  store,
  router,
  render: h => h(App),
  created() {
    // 获取用户菜单信息
    // this.$store.dispatch('layout/initializeRoutes')
  }
}).$mount('#app')
