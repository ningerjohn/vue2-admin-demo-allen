const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/v1': {
        target: 'http://backapi.allen-demo.com',
        changeOrigin: true,
        // pathRewrite: {          // 重写路径
        //   '^/v1': ""         //  把/api变为空字符串
        // }
      }
    }
  }
})
